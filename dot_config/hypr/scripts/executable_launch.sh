#!/bin/bash
#
#
killall waybar
waybar &

# stop hypridle if running and restart it.
killall hypridle && hypridle &
disown
