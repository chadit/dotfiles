# Initial Setup
# exec ~/set_once.sh

# Config for sway
#
# Read `man 5 sway` for a complete reference.

# user config directory
include $HOME/.config/sway/config.d/*
# only enable this if every app you use is compatible with wayland
# xwayland disable


#################
### AUTOSTART ###
#################

# Execute your favorite apps at launch
exec_always ~/.config/hypr/scripts/autostart.sh &

exec_always dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec_always systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP

# Starting a script to handle idle state and lock the screen
exec_always swayidle -w \
    timeout 300 'swaylock -f -c 000000' \
    timeout 600 'swaymsg "output * dpms off"' \
    resume 'swaymsg "output * dpms on"' \
    before-sleep 'swaylock -f -c 000000'

# Set cursor theme and size
exec_always hyprctl setcursor phinger-cursors-light 32

# Clipboard manager

# Set GTK theme
exec_always gsettings set org.gnome.desktop.interface gtk-theme "Catppuccin-Mocha-Standard-Green-Dark"
exec_always gsettings set org.gnome.desktop.interface color-scheme prefer-dark

# Start applets and notifications
exec_always blueman-applet
exec_always nm-applet --indicator
exec_always dunst # notifications

# Start Waybar and Mako
exec_always waybar
exec_always mako

# Start WayVNC
exec_always wayvnc --config ~/.config/wayvnc/config

output eDP-1 {
    scale 1
}

input "2:7:SynPS/2_Synaptics_TouchPad" {
    tap enabled
    natural_scroll enabled  # Enable natural scrolling if desired
    dwt enabled             # Disable while typing
    middle_emulation disable  # Enable middle click emulation
    click_method clickfinger  # Use clickfinger method for clicks
    scroll_method two_finger   # Use two-finger scrolling
}

# Lenovo carbon gen 1
input "2:10:TPPS/2_IBM_TrackPoint" {
    accel_profile flat  # Set acceleration profile to flat
    pointer_accel 0.5   # Adjust pointer acceleration, range is usually -1.0 to 1.0
    natural_scroll enabled  # Enable natural scrolling if desired
    middle_emulation enabled  # Enable middle button emulation
    scroll_method on_button_down  # Use middle button for scrolling
}
