echo "Running on macOS"
# This script installs the necessary dependencies for running Ansible on macOS.
# It checks if Xcode Command Line Tools, Homebrew, and Ansible are installed.
# If they are not installed, it installs them.
# Check if Xcode Command Line Tools are installed
if ! command -v xcode-select >/dev/null 2>&1; then
  echo "Xcode Command Line Tools are not installed. Installing now..."
  # Install Xcode Command Line Tools
  xcode-select --install
else
  echo "Xcode Command Line Tools are already installed."
fi
# Check and install Homebrew
if ! command -v brew >/dev/null 2>&1; then
  echo "Homebrew is not installed. Installing Homebrew now..."
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi
# Check and install Ansible
if ! command -v ansible >/dev/null 2>&1; then
  echo "Ansible is not installed. Installing now..."
  brew install ansible
fi
# execute the ansible playbook
# ansible-playbook ./bootstrap/bootstrap.yml --ask-become-pass

# check and install go for arm
source ./scripts/go.sh
__go_update_macos_arm
