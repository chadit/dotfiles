#!/bin/zsh

function ssh_setup() {
  eval $(ssh-agent -s) >/dev/null 2>&1

  # Directory containing SSH keys
  SSH_DIR="$HOME/.ssh"

  if [ -d "$HOME/.ssh" ]; then
    chmod 700 ~/.ssh
  fi

  if test -f "$HOME/.ssh/authorized_keys"; then
    chmod 644 ~/.ssh/authorized_keys
  fi

  find "$SSH_DIR" -type f -name "*.pub" | while read -r pub_file; do
      # Set permissions for the public key
      chmod 644 "$pub_file"
      # echo "Set permissions 644 for $pub_file"
  
      # Get the associated private key (remove .pub from the filename)
      private_key="${pub_file%.pub}"
  
      # Check if the private key exists
      if [[ -f "$private_key" ]]; then
          # Set permissions for the private key
          chmod 600 "$private_key"
          #echo "Set permissions 600 for $private_key"
  
          # Add the private key to the ssh-agent
          ssh-add "$private_key" > /dev/null 2>&1 # && echo "Added $private_key to ssh-agent" || echo "Failed to add $private_key to ssh-agent"
      else
          echo "No private key found for $pub_file"
      fi
  done  
}