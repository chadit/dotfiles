# Function to download dotnet-install.sh if it does not exist
download_dotnet_install_script() {
  local CURRENTDIR=$(pwd)
  cd ~
  if [ ! -f "dotnet-install.sh" ]; then
    echo "dotnet-install.sh not found. Downloading..."
    wget https://dot.net/v1/dotnet-install.sh -O dotnet-install.sh
    chmod +x dotnet-install.sh
  else
    echo "dotnet-install.sh already exists."
  fi
  cd $CURRENTDIR
}

update_dotnet_latest() {
  local CURRENTDIR=$(pwd)
  cd ~
  if [ -f "dotnet-install.sh" ]; then
    ./dotnet-install.sh --version latest
  fi
  cd $CURRENTDIR
} 

dotnet_setup() {
  if [ -d $HOME/.dotnet ]; then 
    #echo "dotnet set root and add to path."
    export DOTNET_ROOT=$HOME/.dotnet
    export PATH=$PATH:$DOTNET_ROOT:$DOTNET_ROOT/tools
  fi
}