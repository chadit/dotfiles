# How to use

- update .chezmoi.tmol.tmpl with your own tempate changes based on your needs.
- run `chezmio init` to apply any changes to the `~/.config/chezmoi/chezmoi.toml` file.
- run `chezmoi apply` to apply the changes to your home directory.

chezmoi init --apply --verbose https://gitlab.com/chadit/dotfiles.git

sh -c "$(curl -fsLS get.chezmoi.io)" -- init --apply https://gitlab.com/chadit/dotfiles.git


## icon list

https://starship.rs/presets/nerd-font

## Remote desktop manual setup

- install rustdesk as a service (install from the rustdesk website should auto setup the service on linux).
- update the service file with access password and enable linux headless.
- headless monitor should already be setup in the hyprland.conf file.

```bash
[Unit]
Description=RustDesk
Requires=network.target
After=systemd-user-sessions.service

[Service]
Type=simple
Environment="XDG_RUNTIME_DIR=/run/users/$(id -u <user_name>)"
Environment="XDG_SESSION_TYPE=wayland"
Environment="WAYLAND_DISPLAY=wayland-0"
ExecStart=/usr/bin/rustdesk --service --password <password> --option allow-linux-headless Y
PIDFile=/run/rustdesk.pid
KillMode=mixed
TimeoutStopSec=30
User=root
LimitNOFILE=100000

[Install]
WantedBy=multi-user.target

sudo systemctl daemon-reload
sudo systemclt restart rustdesk.service

sudo vim /etc/systemd/logind.conf
# uncomment the line and set the value
[Login]
HandleLidSwitch=ignore

sudo nano /etc/acpi/actions/lid.sh
# add the following

#!/bin/bash
export XDG_RUNTIME_DIR=/run/user/$(id -u)
export WAYLAND_DISPLAY=wayland-0 # Ensure this matches your Wayland display, echo from the terminal to find out.
wlr-randr --output eDP-1 --off

sudo nano /etc/acpi/events/lid
# add the following
event=button/lid.*
action=/etc/acpi/actions/lid.sh

sudo systemctl daemon-reload
sudo systemctl restart acpid
sudo systemctl restart logind
```

## optimizations

```bash
sudo nano /etc/systemd/journald.conf

# in [Journal], will set /tmp to clear on reboot.
Storage=volatile 

sudo nano /etc/tmpfiles.d/tmp.conf

# add the following, sets the retention to 3 days.
d /tmp 1777 root root 3d

```




## TODO:

- [ ] Add a script that fetches ssh keys from bitwarden
- [ ] Add a script that fetches gpg keys from bitwarden
- [ ] update the secrets manager to call bitwarden directly vs the cli tool calls (although I suspect it calls it from the cli tool as well)
- [ ] add dummy packages for compiled items

mkdir -p ~/builds/dummy-wayland && cd ~/builds/dummy-wayland
nano PKGBUILD

pkgname=dummy-wayland
pkgver=1.0
pkgrel=1
pkgdesc="Dummy package to satisfy Wayland dependency"
arch=('any')
provides=('wayland')
conflicts=('wayland')
replaces=('wayland')
license=('MIT')
build() {
    :
}
package() {
    :
}

makepkg -si --noconfirm
#verify the package is installed
pacman -Qi dummy-wayland

yay -Syu

