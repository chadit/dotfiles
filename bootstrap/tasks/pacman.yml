- name: Debug user and home directory
  debug:
    msg: "Home: {{ lookup('env', 'HOME') }}"
  become: false
  become_user: "{{ lookup('env', 'USER') }}"
- name: Install base-devel and git for yay
  pacman:
    name:
      - base
      - base-devel
      - git
    state: present
- name: Check if yay is installed
  command: which yay
  register: yay_installed
  failed_when: false
  changed_when: false
  become: false
- name: Clone yay from AUR
  git:
    repo: "https://aur.archlinux.org/yay.git"
    dest: "~/yay"
  when: yay_installed.rc != 0
  become_user: "{{ lookup('env', 'USER') }}"
  become: false
- name: Build and install yay
  shell: makepkg -si --noconfirm
  args:
    chdir: "~/yay"
  when: yay_installed.rc != 0
  become: false
  become_user: "{{ lookup('env', 'USER') }}"
# ---------------------
# Add dependencies here
# ---------------------
- name: Check if Sublime Text GPG key is already added
  shell: "pacman-key --list-keys | grep -q '8A8F901A'"
  register: sublime_key_added
  ignore_errors: true
- name: Add Sublime Text GPG key if not already added
  shell: |
    curl -O https://download.sublimetext.com/sublimehq-pub.gpg
    pacman-key --add sublimehq-pub.gpg
    pacman-key --lsign-key 8A8F901A
    rm sublimehq-pub.gpg
  when: sublime_key_added.rc != 0
- name: Check if Sublime Text repository is already in pacman.conf
  shell: "grep -q 'Server = https://download.sublimetext.com/arch/stable/x86_64' /etc/pacman.conf"
  register: sublime_repo_added
  ignore_errors: true
- name: Add Sublime Text repository to pacman.conf if not already present
  lineinfile:
    path: /etc/pacman.conf
    line: |
      [sublime-text]
      Server = https://download.sublimetext.com/arch/stable/x86_64
    insertafter: EOF
  when: sublime_repo_added.rc != 0
- name: Install AUR packages using yay
  # ----------------------------------
  command: yay -S --noconfirm {{ item }}
  become: false
  become_user: "{{ ansible_env.USER }}" # Ensure this runs as the normal user
  loop:
    - acpi # acpi client
    - acpid # acpi daemon
    - base-devel # base development tools
    - blueman # bluetooth manager for wayland.
    - bluez # bluetooth stack
    - bluez-cups # bluetooth printer support
    - bluez-utils # bluetooth utilities
    - breeze-icons # icon theme
    - brightnessctl # backlight control
    - btop # top like tool
    - chezmoi
    - clang
    - cmake
    - cpio # hyprland plugin depend
    - curl
    - dart-sass
    - docbook-xsl # xsl stylesheets for docbook
    - docker
    - docker-buildx
    - docker-compose
    - docker-machine
    - doxygen # documentation generator
    - dunst # notification daemon for Wayland
    - file-roller # archive manager for gnome
    - firefox # web browser
    - flameshot # screenshot tool
    - flake8
    - fuse3 # file system in user space
    - fuse-overlayfs # overlay file system
    - fwupd # firmware update daemon
    - gcc-libs # gcc libraries
    - gdk-pixbuf2 # image loading library
    - git # version control
    - git-lfs # git large file storage
    - glib2 # general purpose library
    - glibc # GNU C library
    - glxinfo # gather information about the system
    - gnutls # gnu tls library
    - graphviz # graph visualization
    - grim # wayland screenshot tool
    - gtk3 # gtk3 toolkit
    - gtk4 # gtk4 toolkit
    - gtk-layer-shell # wayland layer shell
    - gtk-engine-murrine # gtk theme engine
    - gvfs # gnome virtual file system
    - imagemagick # image manipulation
    - jansson # json library
    - jq # json processor
    # - kitty # terminal emulator #install from shell script
    - kubectl # kubernetes cli
    - libadwaita # gnome blocks GTK 4
    - libdbusmenu-gtk3 # dbus menu library
    - libclc # OpenCL C library
    - libglvnd # OpenGL vendor-neutral dispatch library
    - libxkbcommon # xkbcommon library
    - linux-headers # linux kernel headers
    - lldb # debugger
    - llvm # compiler infrastructure
    - llvm-libs # llvm libraries
    - luacheck # lua linter
    - luarocks # lua package manager
    - make # build tool
    - mako # wayland notification daemon
    - meson # build system
    - mypy
    - mpv # media player
    - nano # text editor
    - nautilus # file manager for gnome, swap if something else, most installed for hyprland.
    - networkmanager # network manager
    - network-manager-applet # network manager applet for gnome
    - ninja # build system
    - nm-connection-editor # network manager connection editor
    - noto-fonts # google fonts
    - noto-fonts-emoji # emoji font
    - opencv # computer vision library
    - pacman-contrib
    - papirus-icon-theme # icon theme
    - pamixer # pulseaudio mixer cli
    - pango # text layout library
    - pavucontrol # pulseaudio volume control
    - pixman
    - polkit-gnome # polkit agent for gnome
    - python-black
    - python-click # python cli creator
    - python-cmake-build-extension
    - python-gitpython # python git bindings
    - python-gobject # python GObject bindings
    - python-numpy # python numerical computing
    - python-opencv # python computer vision
    - python-pip # python package installer
    - python-psutil # python system utilities
    - python-pylint # python linter
    - python-pylint-venv # python linter venv
    - python-pytest # python testing framework
    - python-pytest-isort # python testing framework
    - python-pywal # color scheme generator
    - python-requests # python http library
    - python-rich # rich text formatting
    - python-virtualenv # python virtual environment
    - qalculate-gtk # GTK frontend for Qalculate
    - rofi # launcher wayland
    - rofi-emoji # emoji rofi
    - rsync # file sync tool
    - rust-bingen # rust binary generator
    - sassc # sass compiler
    - syncthing # file sync tool
    - slurp # wayland region selector (good for grim)
    - spirv-llvm-translator # SPIR-V to LLVM translator
    - sublime-text
    - swappy # wayland native snapshot tool
    - swaybg # sway wallpaper tool
    - timg # terminal image viewer
    - ttf-font-awesome # font awesome
    - ttf-hack-nerd # nerd font
    - tmux # terminal multiplexer
    - tumbler # thumbnailer for xfce/gnome
    - unzip
    - vivaldi # web browser
    - vlc # media player
    - waybar # wayland bar
    - wayvnc # wayland vnc
    - wget
    - wlroots # wayland compositor library
    - wlr-randr # wayland randr tool
    - wofi # Wofi launcher
    # - xclip
    - xmlto # xml to other formats
    - xdg-desktop-portal # desktop portal
    - xdg-user-dirs # Create default user directories
    - zsh
    - catppuccin-gtk-theme-macchiato # GTK theme
    - catppuccin-gtk-theme-mocha # GTK theme
    - cmake-format # cmake formatter
    - gitlint # git commit message linter
    - google-cloud-sdk # Google Cloud SDK
    - kora-icon-theme # icon theme
    - nwg-look # GTK theme editor for wayland
    - otf-openmoji # open source emoji font
    - pm-utils # power management utilities
    - python-pytest-pylint # pytest linter
    - realvnc-vnc-viewer # VNC viewer
    - vcpkg # C++ package manager -- manually run sudo gpasswd -a $USER vcpkg
    - wayland # wayland display server
    - wayland-protocols # wayland protocols
    - wlogout # Sway logout tool if gpg key follow https://github.com/ArtsyMacaw/wlogout/issues/35
    # - wttrbar # weather bar for waybar installing from rust
- name: Update all packages using yay
  become: true # Use elevated privileges if necessary
  ansible.builtin.command:
    cmd: yay -Syyu --noconfirm
# docker setup, is part of the install packages with pacman but this is the setup steps.
# - name: Update all pacman packages
#   ansible.builtin.pacman:
#     update_cache: yes
#     upgrade: yes
# - name: Install packages with pacman
#   become: true
#   ansible.builtin.pacman:
#     name:
#       - acpi # acpi client
#       - acpid # acpi daemon
#       - base-devel # base development tools
#       - blueman # bluetooth manager for wayland.
#       - bluez # bluetooth stack
#       - bluez-cups # bluetooth printer support
#       - bluez-utils # bluetooth utilities
#       - breeze-icons # icon theme
#       - brightnessctl # backlight control
#       - btop # top like tool
#       - chezmoi
#       - clang
#       - cmake
#       - cpio # hyprland plugin depend
#       - curl
#       - dart-sass
#       - docker
#       - docker-buildx
#       - docker-compose
#       - docker-machine
#       - dunst # notification daemon for Wayland
#       - file-roller # archive manager for gnome
#       - firefox # web browser
#       - flameshot # screenshot tool
#       - flake8
#       - fuse3 # file system in user space
#       - fuse-overlayfs # overlay file system
#       - fwupd # firmware update daemon
#       - gcc-libs # gcc libraries
#       - gdk-pixbuf2 # image loading library
#       - git # version control
#       - git-lfs # git large file storage
#       - glib2 # general purpose library
#       - glibc # GNU C library
#       - glxinfo # gather information about the system
#       - gnutls # gnu tls library
#       - grim # wayland screenshot tool
#       - gtk3 # gtk3 toolkit
#       - gtk4 # gtk4 toolkit
#       - gtk-layer-shell # wayland layer shell
#       - gtk-engine-murrine # gtk theme engine
#       - gvfs # gnome virtual file system
#       - imagemagick # image manipulation
#       - jansson # json library
#       - jq # json processor
#       # - kitty # terminal emulator #install from shell script
#       - kubectl # kubernetes cli
#       - libadwaita # gnome blocks GTK 4
#       - libdbusmenu-gtk3 # dbus menu library
#       - libglvnd # OpenGL vendor-neutral dispatch library
#       - libxkbcommon # xkbcommon library
#       - linux-headers # linux kernel headers
#       - lldb # debugger
#       - luacheck # lua linter
#       - luarocks # lua package manager
#       - make # build tool
#       - mako # wayland notification daemon
#       - meson
#       - mypy
#       - mpv # media player
#       - nano # text editor
#       - nautilus # file manager for gnome, swap if something else, most installed for hyprland.
#       - networkmanager # network manager
#       - network-manager-applet # network manager applet for gnome
#       - ninja
#       - nm-connection-editor # network manager connection editor
#       - noto-fonts # google fonts
#       - noto-fonts-emoji # emoji font
#       - opencv # computer vision library
#       - pacman-contrib
#       - papirus-icon-theme # icon theme
#       - pamixer # pulseaudio mixer cli
#       - pango # text layout library
#       - pavucontrol # pulseaudio volume control
#       - pixman
#       - polkit-gnome # polkit agent for gnome
#       - python-black
#       - python-click # python cli creator
#       - python-cmake-build-extension
#       - python-gitpython # python git bindings
#       - python-gobject # python GObject bindings
#       - python-numpy # python numerical computing
#       - python-opencv # python computer vision
#       - python-pip # python package installer
#       - python-psutil # python system utilities
#       - python-pylint # python linter
#       - python-pylint-venv # python linter venv
#       - python-pytest # python testing framework
#       - python-pytest-isort # python testing framework
#       - python-pywal # color scheme generator
#       - python-requests # python http library
#       - python-rich # rich text formatting
#       - python-virtualenv # python virtual environment
#       - qalculate-gtk # GTK frontend for Qalculate
#       - rofi # launcher wayland
#       - rofi-emoji # emoji rofi
#       - rsync # file sync tool
#       - sassc # sass compiler
#       - syncthing # file sync tool
#       - slurp # wayland region selector (good for grim)
#       - sublime-text
#       - swappy # wayland native snapshot tool
#       - swaybg # sway wallpaper tool
#       - ttf-hack-nerd # nerd font
#       - tmux # terminal multiplexer
#       - tumbler # thumbnailer for xfce/gnome
#       - unzip
#       - vivaldi # web browser
#       - vlc # media player
#       - waybar # wayland bar
#       - wayvnc # wayland vnc
#       - wget
#       - wlroots # wayland compositor library
#       - wlr-randr # wayland randr tool
#       - wofi # Wofi launcher
#       - xclip
#       - xdg-desktop-portal # desktop portal
#       - xdg-user-dirs # Create default user directories
#       - zsh
#     state: present
# - name: Start and enable Docker service
#   systemd:
#     name: docker
#     enabled: yes
#     state: started
#   ignore_errors: true # some cloud systems will block docker or it needs to be done a different way.
# - name: Create docker group if it doesn't exist
#   group:
#     name: docker
#     state: present
# - name: Check if user is already in docker group
#   shell: "groups {{ ansible_user }} | grep -q '\\bdocker\\b'"
#   register: user_in_docker_group
#   ignore_errors: true
# - name: Add user to docker group if not already a member
#   user:
#     name: "{{ ansible_user }}"
#     groups: docker
#     append: yes
#   when: user_in_docker_group.rc != 0
# - name: Notify user to log out and back in if added to docker group
#   debug:
#     msg: "The user {{ ansible_user }} has been added to the docker group. Please log out and back in for the changes to take effect."
#   when: user_in_docker_group.rc != 0
