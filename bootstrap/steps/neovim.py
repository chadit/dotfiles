import os
import subprocess
import requests
from pathlib import Path
import tarfile


def install_neovim():
    # Fetch the latest Neovim release
    release_url = "https://api.github.com/repos/neovim/neovim/releases/latest"
    response = requests.get(release_url)
    response.raise_for_status()
    latest_release = response.json()
    latest_version = latest_release["tag_name"]

    # Check if Neovim is already installed and up-to-date
    try:
        current_version_output = subprocess.run(
            ["nvim", "--version"], capture_output=True, text=True, check=True
        )
        current_version = current_version_output.stdout.split()[1]

        # If the current version is the latest, skip the installation
        if current_version == latest_version:
            print(f"Neovim is already up-to-date (version {current_version}).")
            return
        else:
            print(
                f"Updating Neovim from version {current_version} to {latest_version}..."
            )
    except FileNotFoundError:
        print("Neovim is not installed. Proceeding with installation...")

    # Find the download URL for the Linux tarball
    asset_url = None
    for asset in latest_release["assets"]:
        if "nvim-linux-x86_64.tar.gz" in asset["name"]:
            asset_url = asset["browser_download_url"]
            break

    if not asset_url:
        print("Could not find a Linux binary for the latest Neovim release.")
        return

    # Download the tarball
    tarball_path = Path("/tmp/neovim.tar.gz")
    with requests.get(asset_url, stream=True) as r:
        r.raise_for_status()
        with open(tarball_path, "wb") as f:
            for chunk in r.iter_content(chunk_size=8192):
                f.write(chunk)

    # Extract the tarball
    with tarfile.open(tarball_path, "r:gz") as tar:
        tar.extractall("/tmp/neovim")

    # Move Neovim files to /usr
    nvim_bin_path = "/tmp/neovim/nvim-linux64/bin/nvim"
    if os.path.exists(nvim_bin_path):
        try:
            # Ensure /usr/bin is writable
            subprocess.run(["sudo", "mv", nvim_bin_path, "/usr/bin/nvim"], check=True)
            print("Neovim installed successfully in /usr/bin.")
        except subprocess.CalledProcessError:
            print(
                "Failed to move Neovim binary to /usr/bin. Make sure you have sudo privileges."
            )
    else:
        print("Neovim binary not found after extraction.")

    # Clean up
    os.remove(tarball_path)
    print("Temporary files cleaned up.")


def main():
    install_neovim()


if __name__ == "__main__":
    main()
