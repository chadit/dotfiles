import json
import os


class PersistentDictionary:
    def __init__(self, file_path):
        """Initialize the PersistentDictionary."""
        self.file_path = file_path
        self.data = self.load()

    def load(self):
        """Load the dictionary from the flat file."""
        if os.path.exists(self.file_path):
            with open(self.file_path, "r") as file:
                try:
                    return json.load(file)
                except json.JSONDecodeError:
                    print(
                        "File exists but is corrupted. Starting with an empty dictionary."
                    )
                    return {}
        else:
            print("File not found. Starting with an empty dictionary.")
            return {}

    def save(self):
        """Save the dictionary to the flat file."""
        with open(self.file_path, "w") as file:
            json.dump(self.data, file, indent=4)

    def get(self, key, default=None):
        """Get a value from the dictionary."""
        return self.data.get(key, default)

    def set(self, key, value):
        """Set a value in the dictionary."""
        self.data[key] = value
        self.save()

    def delete(self, key):
        """Delete a key from the dictionary."""
        if key in self.data:
            del self.data[key]
            self.save()

    def keys(self):
        """Get all keys in the dictionary."""
        return self.data.keys()

    def values(self):
        """Get all values in the dictionary."""
        return self.data.values()

    def items(self):
        """Get all items in the dictionary."""
        return self.data.items()

    def clear(self):
        """Clear the dictionary."""
        self.data.clear()
        self.save()
