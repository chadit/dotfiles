import os
import subprocess
import sys
from pathlib import Path
import re
import platform
import requests
import settings


# ubuntu dependencies
# sudo apt install meson libdrm-dev libxkbcommon-dev libwlroots-dev libjansson-dev libpam0g-dev libgnutls28-dev libavfilter-dev libavcodec-dev libavutil-dev libturbojpeg0-dev scdoc
# sudo apt install clang-tidy gobject-introspection libdbusmenu-gtk3-dev libevdev-dev libfmt-dev libgirepository1.0-dev libgtk-3-dev libgtkmm-3.0-dev libinput-dev libjsoncpp-dev libmpdclient-dev libnl-3-dev libnl-genl-3-dev libpulse-dev libsigc++-2.0-dev libspdlog-dev libwayland-dev scdoc upower libxkbregistry-dev
# sudo apt install libglib2.0-dev catch2 mesa-utils
# sudo apt install -y meson wget build-essential ninja-build cmake-extras cmake gettext gettext-base fontconfig libfontconfig-dev libffi-dev libxml2-dev libdrm-dev libxkbcommon-x11-dev libxkbregistry-dev libxkbcommon-dev libpixman-1-dev libudev-dev  seatd libxcb-dri3-dev libegl-dev libgles2 libegl1-mesa-dev glslang-tools libinput-bin libinput-dev libxcb-composite0-dev libavutil-dev libavcodec-dev libavformat-dev libxcb-ewmh2 libxcb-ewmh-dev libxcb-present-dev libxcb-icccm4-dev libxcb-render-util0-dev libxcb-res0-dev libxcb-xinput-dev libtomlplusplus3
# ngcc-13, g++-13 or 14 is needed

# sudo add-apt-repository ppa:ubuntu-toolchain-r/test
# sudo apt update
# sudo apt install gcc-14 g++-14
# set as default
# sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-14 100
# sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-14 100


# arch dependencies (use yay or paru)
# pacman -S base-devel libglvnd libxkbcommon pixman gnutls jansson
# pacman -S glib2 glib2-devel catch2

# gentoo dependencies


# TODO: build mesa from source. and maybe libdrm

# Define the repository URLs for each package
# messes up because aur is a pain to manage dependencies.
# install hyprland via aur or distro package manager.
PACKAGES = {
    # libraries
    # "aquamarine-git": "https://github.com/hyprwm/aquamarine.git",
    # "wlroots-git": "https://gitlab.freedesktop.org/wlroots/wlroots.git",
    # "hyprlang-git": "https://github.com/hyprwm/hyprlang.git",
    # "hyprcursor-git": "https://github.com/hyprwm/hyprcursor.git",
    # "hyprutils-git": "https://github.com/hyprwm/hyprutils.git",
    # "hyprland-protocols-git": "https://github.com/hyprwm/hyprland-protocols.git",
    # "xdg-desktop-portal-hyprland-git": "https://github.com/hyprwm/xdg-desktop-portal-hyprland.git",
    # # "libdisplay-info-git": "https://gitlab.freedesktop.org/emersion/libdisplay-info.git",
    # # "mesa-git": "https://gitlab.freedesktop.org/mesa/mesa.git",
    # # # # "scenefx-git": "https://github.com/wlrfx/scenefx.git", -- disabled for now, does not work with wlroot > 0.17, removed from other systems and seems to be okay.
    # # # Binaries in /usr/bin
    # "hyprgraphics-git": "https://github.com/hyprwm/hyprgraphics.git",
    # "mako-git": "https://github.com/emersion/mako.git",
    # "hypridle-git": "https://github.com/hyprwm/hypridle.git",
    # "hyprlock-git": "https://github.com/hyprwm/hyprlock.git",
    # "hyprshot-git": "https://github.com/Gustash/Hyprshot.git",
    # "hyprwayland-scanner-git": "https://github.com/hyprwm/hyprwayland-scanner.git",
    # "wayvnc-git": "https://github.com/any1/wayvnc.git",
    # "waybar-git": "https://github.com/Alexays/Waybar.git",
    # "wl-clipboard-git": "https://github.com/bugaevc/wl-clipboard.git",
    # "wlogout-git": "https://github.com/ArtsyMacaw/wlogout.git",
    # "wlr-randr-git": "https://git.sr.ht/~emersion/wlr-randr",
    # "swaybg-git": "https://github.com/swaywm/swaybg.git",
    # Themes
    # "kora-icon-theme": "https://github.com/bikass/kora.git",
    # "nerd-fonts-git": "https://github.com/ryanoasis/nerd-fonts.git",  # -- disabled for now, it is really large repo.
    # "otf-openmoji": "https://github.com/hfg-gmuend/openmoji.git",
    # #
    # # Main Application
    # "hyprland-git": "https://github.com/hyprwm/Hyprland.git",
}

# Directory to clone repositories
BUILD_DIR = Path.home() / "source_repos"


def get_latest_mesa_version():
    url = "https://gitlab.freedesktop.org/api/v4/projects/176/projects/176/repository/tags"
    response = requests.get(url)
    if response.status_code == 200:
        tags = response.json()
        for tag in tags:
            if tag["name"].startswith("mesa-"):
                print("Latest Mesa version:", tag["name"].lstrip("mesa-"))
                return
    else:
        print("Failed to fetch Mesa versions.")


def run_command(command, cwd=None):
    """Run a shell command and print its output in real-time."""
    process = subprocess.Popen(
        command,
        shell=True,
        cwd=cwd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )

    # Print stdout in real-time
    for line in process.stdout:
        print(line, end="")

    # Print stderr in real-time
    for line in process.stderr:
        print(line, end="")

    process.stdout.close()
    process.stderr.close()
    return_code = process.wait()

    if return_code != 0:
        print(f"Error running command: {command}")
        sys.exit(1)

    return_code


def clone_repo(package, url):
    """Clone the repository for a package or update it if it already exists."""
    package_dir = BUILD_DIR / package

    # remove wayvnc it has build dependancies that cause issues, easier to start fresh.
    if package == "wayvnc-git":
        if package_dir.exists():
            run_command(f"rm -rf {package_dir}")

    if package_dir.exists():
        print(f"{package} already cloned. Pulling latest changes...")
        reset_repo(package_dir)
        try:
            # Ensure the directory is a Git repository
            subprocess.run(
                ["git", "-C", str(package_dir), "rev-parse", "--is-inside-work-tree"],
                check=True,
                capture_output=True,
            )

            # Pull latest changes and perform maintenance tasks
            subprocess.run(["git", "-C", str(package_dir), "pull"], check=True)
            subprocess.run(["git", "-C", str(package_dir), "prune"], check=True)
            # subprocess.run(
            #     ["git", "-C", str(package_dir), "gc", "--aggressive"], check=True
            # )
            print(f"{package} updated successfully.")
        except subprocess.CalledProcessError as e:
            print(f"Failed to update {package}: {e}")
    else:
        print(f"Cloning {package} from {url}...")
        # Custom clone handler
        if package == "hyprland-git":
            run_command(f"git clone --recursive {url} {package_dir}")
        elif package == "mesa-git":
            run_command(f"git clone --depth=1 {url} {package_dir}")
        else:
            run_command(f"git clone {url} {package_dir}")
    return package_dir


# start cleanup
def cleanup_old_aquamarine_versions():
    """Remove older versions of libaquamarine and update symlinks to the latest version."""
    # Find all versions of libaquamarine in /usr/lib
    lib_files = sorted(Path("/usr/lib").glob("libaquamarine.so.*"))

    # Ensure there is more than one version; otherwise, there's nothing to clean up
    if len(lib_files) <= 1:
        print("No old versions to clean up.")
        return

    # Extract version numbers and identify the latest version
    version_pattern = re.compile(r"libaquamarine\.so\.(\d+\.\d+\.\d+)")
    versions = [
        (lib, version_pattern.search(lib.name).group(1))
        for lib in lib_files
        if version_pattern.search(lib.name)
    ]
    versions.sort(
        key=lambda x: [int(num) for num in x[1].split(".")]
    )  # Sort by version numbers

    # The last item in the sorted list is the latest version
    latest_lib, latest_version = versions[-1]

    # Remove all but the latest version
    for lib, version in versions[:-1]:
        print(f"Removing old version: {lib}")
        run_command(f"sudo rm {lib}")

    # Update symlink to point to the latest version
    latest_symlink = Path("/usr/lib/libaquamarine.so.3")
    print(f"Updating symlink {latest_symlink} to point to {latest_lib}")
    run_command(f"sudo ln -sf {latest_lib} {latest_symlink}")


def cleanup_old_hyprcursor_versions():
    """Remove older versions of libhyprcursor and update symlinks to the latest version."""
    # Find all versions of libhyprcursor in /usr/lib
    lib_files = sorted(Path("/usr/lib").glob("libhyprcursor.so.*"))

    # Ensure there is more than one version; otherwise, there's nothing to clean up
    if len(lib_files) <= 1:
        print("No old versions to clean up.")
        return

    # Extract version numbers and identify the latest version
    version_pattern = re.compile(r"libhyprcursor\.so\.(\d+\.\d+\.\d+)")
    versions = [
        (lib, version_pattern.search(lib.name).group(1))
        for lib in lib_files
        if version_pattern.search(lib.name)
    ]
    versions.sort(
        key=lambda x: [int(num) for num in x[1].split(".")]
    )  # Sort by version numbers

    # The last item in the sorted list is the latest version
    latest_lib, latest_version = versions[-1]

    # Remove all but the latest version
    for lib, version in versions[:-1]:
        print(f"Removing old version: {lib}")
        run_command(f"sudo rm {lib}")

    # Update symlinks to point to the latest version
    latest_symlink_0 = Path("/usr/lib/libhyprcursor.so.0")
    latest_symlink = Path("/usr/lib/libhyprcursor.so")

    print(f"Updating symlink {latest_symlink_0} to point to {latest_lib}")
    run_command(f"sudo ln -sf {latest_lib} {latest_symlink_0}")

    print(f"Updating symlink {latest_symlink} to point to {latest_symlink_0}")
    run_command(f"sudo ln -sf {latest_symlink_0} {latest_symlink}")


def cleanup_old_hyprlang_versions():
    """Remove older versions of libhyprlang and update symlinks to the latest version."""
    # Find all versions of libhyprlang in /usr/lib
    lib_files = sorted(Path("/usr/lib").glob("libhyprlang.so.*"))

    # Ensure there is more than one version; otherwise, there's nothing to clean up
    if len(lib_files) <= 1:
        print("No old versions to clean up.")
        return

    # Extract version numbers and identify the latest version
    version_pattern = re.compile(r"libhyprlang\.so\.(\d+\.\d+\.\d+)")
    versions = [
        (lib, version_pattern.search(lib.name).group(1))
        for lib in lib_files
        if version_pattern.search(lib.name)
    ]
    versions.sort(
        key=lambda x: [int(num) for num in x[1].split(".")]
    )  # Sort by version numbers

    # The last item in the sorted list is the latest version
    latest_lib, latest_version = versions[-1]

    # Remove all but the latest version
    for lib, version in versions[:-1]:
        print(f"Removing old version: {lib}")
        run_command(f"sudo rm {lib}")

    # Update symlinks to point to the latest version
    latest_symlink_main = Path("/usr/lib/libhyprlang.so.2")
    latest_symlink_generic = Path("/usr/lib/libhyprlang.so")

    print(f"Updating symlink {latest_symlink_main} to point to {latest_lib}")
    run_command(f"sudo ln -sf {latest_lib} {latest_symlink_main}")

    print(
        f"Updating symlink {latest_symlink_generic} to point to {latest_symlink_main}"
    )
    run_command(f"sudo ln -sf {latest_symlink_main} {latest_symlink_generic}")


def cleanup_old_hyprutils_versions():
    """Remove older versions of libhyprutils and update symlinks to the latest version."""
    # Find all versions of libhyprutils in /usr/lib
    lib_files = sorted(Path("/usr/lib").glob("libhyprutils.so.*"))

    # Ensure there is more than one version; otherwise, there's nothing to clean up
    if len(lib_files) <= 1:
        print("No old versions to clean up.")
        return

    # Extract version numbers and identify the latest version
    version_pattern = re.compile(r"libhyprutils\.so\.(\d+\.\d+\.\d+)")
    versions = [
        (lib, version_pattern.search(lib.name).group(1))
        for lib in lib_files
        if version_pattern.search(lib.name)
    ]
    versions.sort(
        key=lambda x: [int(num) for num in x[1].split(".")]
    )  # Sort by version numbers

    # The last item in the sorted list is the latest version
    latest_lib, latest_version = versions[-1]

    # Remove all but the latest version
    for lib, version in versions[:-1]:
        print(f"Removing old version: {lib}")
        run_command(f"sudo rm {lib}")

    # Update symlinks to point to the latest version
    latest_symlink_main = Path("/usr/lib/libhyprutils.so.1")
    latest_symlink_generic = Path("/usr/lib/libhyprutils.so")

    print(f"Updating symlink {latest_symlink_main} to point to {latest_lib}")
    run_command(f"sudo ln -sf {latest_lib} {latest_symlink_main}")

    print(
        f"Updating symlink {latest_symlink_generic} to point to {latest_symlink_main}"
    )
    run_command(f"sudo ln -sf {latest_symlink_main} {latest_symlink_generic}")


def cleanup_old_scenefx_versions():
    """Remove older versions of libscenefx and update symlinks to the latest version."""
    lib_dir = Path("/usr/lib")
    lib_files = sorted(lib_dir.glob("libscenefx.so.*"))

    # Ensure there is more than one version; otherwise, there's nothing to clean up
    if len(lib_files) <= 1:
        print("No old versions to clean up.")
        return

    # Identify the latest version
    version_pattern = re.compile(r"libscenefx\.so\.(\d+)$")
    versions = [
        (lib, int(version_pattern.search(lib.name).group(1)))
        for lib in lib_files
        if version_pattern.search(lib.name)
    ]
    versions.sort(key=lambda x: x[1])  # Sort by version numbers

    # The last item in the sorted list is the latest version
    latest_lib, latest_version = versions[-1]

    # Remove all but the latest version
    for lib, version in versions[:-1]:
        print(f"Removing old version: {lib}")
        run_command(f"sudo rm {lib}")

    # Update symlinks to point to the latest version
    latest_symlink_main = lib_dir / "libscenefx.so.1"
    latest_symlink_generic = lib_dir / "libscenefx.so"

    print(f"Updating symlink {latest_symlink_main} to point to {latest_lib}")
    run_command(f"sudo ln -sf {latest_lib} {latest_symlink_main}")

    print(
        f"Updating symlink {latest_symlink_generic} to point to {latest_symlink_main}"
    )
    run_command(f"sudo ln -sf {latest_symlink_main} {latest_symlink_generic}")


def cleanup_old_display_info_versions():
    """Remove older versions of libdisplay-info and update symlinks to the latest version."""
    # Find all versions of libdisplay-info in /usr/lib
    lib_files = sorted(Path("/usr/lib").glob("libdisplay-info.so.*"))

    # Ensure there is more than one version; otherwise, there's nothing to clean up
    if len(lib_files) <= 1:
        print("No old versions to clean up.")
        return

    # Extract version numbers and identify the latest version
    version_pattern = re.compile(r"libdisplay-info\.so\.(\d+\.\d+\.\d+)")
    versions = [
        (lib, version_pattern.search(lib.name).group(1))
        for lib in lib_files
        if version_pattern.search(lib.name)
    ]
    versions.sort(
        key=lambda x: [int(num) for num in x[1].split(".")]
    )  # Sort by version numbers

    # The last item in the sorted list is the latest version
    latest_lib, latest_version = versions[-1]

    # Remove all but the latest version
    for lib, version in versions[:-1]:
        print(f"Removing old version: {lib}")
        run_command(f"sudo rm {lib}")

    # Update symlinks to point to the latest version
    latest_symlink_main = Path("/usr/lib/libdisplay-info.so.1")
    latest_symlink_generic = Path("/usr/lib/libdisplay-info.so")

    print(f"Updating symlink {latest_symlink_main} to point to {latest_lib}")
    run_command(f"sudo ln -sf {latest_lib} {latest_symlink_main}")

    print(
        f"Updating symlink {latest_symlink_generic} to point to {latest_symlink_main}"
    )
    run_command(f"sudo ln -sf {latest_symlink_main} {latest_symlink_generic}")

    # sudo ln -s /usr/lib/libdisplay-info.so.0.3.0 /usr/lib/libdisplay-info.so.2
    # todo when hyprland is updated to no longer need 2 then this can be removed or updated.
    run_command(f"sudo ln -sf {latest_lib} /usr/lib/libdisplay-info.so.2")


# end cleanup


# start build helpers
def check_dependencies():
    """Check if all dependencies are installed."""

    # Check if the OS is Ubuntu
    os_release = platform.system()
    if os_release != "Linux":
        print("Not running on a Linux system. Exiting.")
        return

    try:
        with open("/etc/os-release") as f:
            os_info = f.read()
    except FileNotFoundError:
        print("Unable to verify OS. Exiting.")
        return

    is_ubuntu = "Ubuntu" in os_info
    is_arch = "Arch Linux" in os_info

    if is_arch:
        print("Arch Linux detected.")
        check_and_install_arch_dependencies()

    if is_ubuntu:
        print("Ubuntu detected.")
        # check_and_install_gcc_gpp_14_ubuntu()


def check_and_install_arch_dependencies():
    """Install Arch Linux dependencies."""

    #  # yay -S ninja gcc cmake meson libxcb xcb-proto xcb-util xcb-util-keysyms libxfixes libx11 libxcomposite libxrender pixman wayland-protocols cairo pango seatd libxkbcommon xcb-util-wm xorg-xwayland libinput libliftoff libdisplay-info cpio tomlplusplus hyprlang-git hyprcursor-git hyprwayland-scanner-git xcb-util-errors hyprutils-git
    #     dependencies_cmd = (
    #         "sudo pacman -Syyu --noconfirm xcb-util-errors tomlplusplus cairo"
    #     )

    commands = [
        "yay -Syyu --noconfirm seatd",  # aquamarine
        "yay -Syyu --noconfirm sdbus-cpp",  # xdg-desktop-portal-hyprland-git
        "yay -Syyu --noconfirm gtkmm3",  # waybar-git
        "yay -Syyu --noconfirm xcb-util-errors tomlplusplus cairo",  # hyprland
        "yay -Syyu --noconfirm mesa-git libdrm-git",  # intel graphics
    ]

    for cmd in commands:
        print(f"Running command: {cmd}")
        result = subprocess.run(cmd, shell=True)
        if result.returncode != 0:
            print(f"arch command failed: {cmd}")
            return


def check_and_install_ubuntu_dependencies():
    """Install GCC and G++ 14 only if the OS is Ubuntu and GCC 14 is not installed."""

    commands = [
        "sudo apt install seatd libseat1 libseat-dev",  # aquamarine
        "sudo apt install libsdbus-c++1",  # xdg-desktop-portal-hyprland-git
        "sudo apt install libgtkmm-3.0-1v5",  # waybar-git
    ]

    for cmd in commands:
        print(f"Running command: {cmd}")
        result = subprocess.run(cmd, shell=True)
        if result.returncode != 0:
            print(f"arch command failed: {cmd}")
            return

    # TODO: need to fix this detection and cleanup
    # # Check if GCC 14 and G++ 14 are installed
    # gcc_installed = (
    #     subprocess.run(
    #         "gcc-14 --version",
    #         shell=True,
    #         stdout=subprocess.PIPE,
    #         stderr=subprocess.PIPE,
    #     ).returncode
    #     == 0
    # )
    # gpp_installed = (
    #     subprocess.run(
    #         "g++-14 --version",
    #         shell=True,
    #         stdout=subprocess.PIPE,
    #         stderr=subprocess.PIPE,
    #     ).returncode
    #     == 0
    # )

    # if gcc_installed and gpp_installed:
    #     print("GCC 14 and G++ 14 are already installed.")
    #     return

    # print("Installing GCC and G++ 14...")

    # # Commands to install prerequisites, download, build, and install GCC 14 from source
    # commands = [
    #     "sudo apt update",
    #     "sudo apt install -y build-essential libgmp-dev libmpfr-dev libmpc-dev zlib1g-dev",
    #     "wget https://ftp.gnu.org/gnu/gcc/gcc-14.2.0/gcc-14.2.0.tar.gz",
    #     "tar -xvf gcc-14.2.0.tar.gz",
    #     "cd gcc-14.2.0 && mkdir build && cd build",
    #     "../configure --enable-languages=c,c++ --disable-multilib --prefix=/usr/local/gcc-14",
    #     "make -j$(nproc)",
    #     "sudo make install",
    # ]

    # for cmd in commands:
    #     result = subprocess.run(cmd, shell=True)
    #     if result.returncode != 0:
    #         print(f"ubuntu command failed: {cmd}")
    #         return

    # print("GCC and G++ 14 installed successfully.")
    # print("To use GCC 14, add /usr/local/gcc-14/bin to your PATH.")
    # sys.exit(0)


def get_latest_wlroots_pc():
    """Find the latest versioned wlroots .pc file in /usr/lib/pkgconfig."""
    pkgconfig_dir = Path("/usr/lib/pkgconfig")
    wlroots_pc_files = list(pkgconfig_dir.glob("wlroots-*.pc"))

    # Parse version numbers and find the highest version
    version_pattern = re.compile(r"wlroots-(\d+\.\d+).pc")
    versions = []

    for pc_file in wlroots_pc_files:
        match = version_pattern.search(pc_file.name)
        if match:
            version = tuple(
                map(int, match.group(1).split("."))
            )  # Convert to tuple of ints for comparison
            versions.append((version, pc_file))

    # Find the .pc file with the highest version
    if versions:
        latest_version = max(versions, key=lambda v: v[0])[1]
        print(f"Latest wlroots .pc file found: {latest_version}")
        return latest_version
    else:
        print("No versioned wlroots .pc files found.")
        return None


def increment_version(version_str, increment=0.02):
    """Increment the version string by a specified amount (default: 0.02) in the minor version."""
    major, minor, patch = map(int, version_str.split("."))
    new_version = f"{major}.{minor + int(increment * 100):02d}.{patch}"
    return new_version


def update_wlroots_version(meson_build_path):
    """Update the wlroots_version range in the meson.build file by adding 0.02 to the upper bound."""

    # Read the current meson.build content
    with open(meson_build_path, "r") as file:
        lines = file.readlines()

    # Regex pattern to match the wlroots_version line
    pattern = re.compile(
        r"wlroots_version\s*=\s*\[\s*'>(=)?([\d.]+)',\s*'(<)?([\d.]+)'\s*\]"
    )

    updated_lines = []
    for line in lines:
        match = pattern.search(line)
        if match:
            # Extract the lower and upper bounds
            lower_version = match.group(2)
            upper_version = match.group(4)

            # Increment the upper version bound
            new_upper_version = increment_version(upper_version)

            # Create the updated line
            updated_line = (
                f"wlroots_version = ['>={lower_version}', '<{new_upper_version}']\n"
            )
            updated_lines.append(updated_line)
            print(f"Updated wlroots version line: {updated_line.strip()}")
        else:
            updated_lines.append(line)

    # Write the updated content back to the meson.build file
    with open(meson_build_path, "w") as file:
        file.writelines(updated_lines)


# end build helpers

# hyprcursor there is also a hyprcurosr-utils todo to find this.


def build_package(package, package_dir):
    """Run the specific build steps for each package."""
    print(f"Building {package}...")

    try:
        with open("/etc/os-release") as f:
            os_info = f.read()
    except FileNotFoundError:
        print("Unable to verify OS. Exiting.")
        return

    if "Ubuntu" in os_info:
        is_ubuntu = True
    else:
        is_ubuntu = False

    # -- Libaries --
    if package == "aquamarine-git":
        # Build steps
        if is_ubuntu:
            print("Ubuntu detected. Using Ubuntu-specific CMake configuration.")
            # Ubuntu-specific CMake configuration
            cmake_configure = (
                "cmake --no-warn-unused-cli "
                "-DCMAKE_BUILD_TYPE:STRING=Release "
                "-DCMAKE_INSTALL_PREFIX:PATH=/usr "
                "-DOpenGL_GLES_LIBRARY=/usr/lib/x86_64-linux-gnu/libGLESv2.so "
                "-S . -B ./build"
            )
        else:
            print("Default CMake configuration for other systems.")
            # Default CMake configuration for other systems
            cmake_configure = "cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_INSTALL_PREFIX:PATH=/usr -S . -B ./build"

        cmake_configure = "cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_INSTALL_PREFIX:PATH=/usr -S . -B ./build"
        cmake_build = "cmake --build ./build --config Release --target all -j$(nproc 2>/dev/null || getconf _NPROCESSORS_CONF)"
        cmake_install = "sudo cmake --install build"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)

        # Cleanup old versions
        cleanup_old_aquamarine_versions()
    elif package == "mesa-git":
        # Build steps
        setup_configure = "meson build --prefix=/usr"
        setup_build = "ninja -C build"
        setup_install = "sudo ninja -C build install"

        run_command(setup_configure, cwd=package_dir)
        run_command(setup_build, cwd=package_dir)
        run_command(setup_install, cwd=package_dir)
    elif package == "hyprland-protocols-git":  # Meson install
        # Build steps
        setup_configure = "meson setup build/ --prefix=/usr"
        setup_build = "meson compile -C build"
        setup_install = "sudo meson install -C build"

        run_command(setup_configure, cwd=package_dir)
        run_command(setup_build, cwd=package_dir)
        run_command(setup_install, cwd=package_dir)
    elif package == "hyprland-git":  # Meson setup with Ninja build/install
        # Build steps
        # setup_reset = "meson subprojects update --reset"
        setup_configure = "meson setup build/ --prefix=/usr"
        setup_build = "ninja -C build"
        setup_install = "sudo meson install -C build --tags runtime,man"

        # run_command(setup_reset, cwd=package_dir)
        run_command(setup_configure, cwd=package_dir)
        run_command(setup_build, cwd=package_dir)
        run_command(setup_install, cwd=package_dir)

    elif package == "wlroots-git":
        # meson *
        # wlroots
        # wayland
        # wayland-protocols *
        # EGL and GLESv2
        # libdrm
        # pixman

        # Build steps
        cmake_configure = (
            "PKG_CONFIG_PATH=/usr/lib/pkgconfig meson setup build/ --prefix=/usr"
        )
        cmake_build = "ninja -C build/"
        cmake_install = "sudo ninja -C build/ install"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)

        # update generic symlink
        latest_wlroots_pc = get_latest_wlroots_pc()
        generic_wlroots_pc = Path("/usr/lib/pkgconfig/wlroots.pc")

        if latest_wlroots_pc and not generic_wlroots_pc.exists():
            print(
                f"Creating symlink for {generic_wlroots_pc} pointing to {latest_wlroots_pc}"
            )
            run_command(f"sudo ln -sf {latest_wlroots_pc} {generic_wlroots_pc}")
        else:
            if not latest_wlroots_pc:
                print("No versioned wlroots.pc file found to create symlink.")
            else:
                print(f"{generic_wlroots_pc} already exists.")

        # Cleanup old versions
    elif package == "hyprlang-git":
        # Build steps
        cmake_configure = "cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_INSTALL_PREFIX:PATH=/usr -S . -B ./build"
        cmake_build = "cmake --build ./build --config Release --target hyprlang -j$(nproc 2>/dev/null || getconf _NPROCESSORS_CONF)"
        cmake_install = "sudo cmake --install build"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)

        # Cleanup old versions
        cleanup_old_hyprlang_versions()
    elif package == "hyprutils-git":
        # Build steps
        cmake_configure = "cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_INSTALL_PREFIX:PATH=/usr -S . -B ./build"
        cmake_build = "cmake --build ./build --config Release --target all -j$(nproc 2>/dev/null || getconf _NPROCESSORS_CONF)"
        cmake_install = "sudo cmake --install build"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)

        # Cleanup old versions
        cleanup_old_hyprutils_versions()
    elif package == "hyprcursor-git":

        # hyprlang >= 0.4.2
        # cairo
        # libzip
        # librsvg
        # tomlplusplus

        # Build steps
        cmake_configure = "cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_INSTALL_PREFIX:PATH=/usr -S . -B ./build"
        cmake_build = "cmake --build ./build --config Release --target all -j$(nproc 2>/dev/null || getconf _NPROCESSORS_CONF)"
        cmake_install = "sudo cmake --install build"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)

        # Cleanup old versions
        cleanup_old_hyprcursor_versions()
    elif package == "scenefx-git":

        # meson *
        # wlroots
        # wayland
        # wayland-protocols *
        # EGL and GLESv2
        # libdrm
        # pixman

        # Step 1: Update wlroots version in meson.build
        meson_build_path = package_dir / "meson.build"
        update_wlroots_version(meson_build_path)

        # Build steps
        cmake_configure = (
            "PKG_CONFIG_PATH=/usr/lib/pkgconfig meson setup build/ --prefix=/usr"
        )
        cmake_build = "ninja -C build/"
        cmake_install = "sudo ninja -C build/ install"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)

        # Cleanup old versions
        cleanup_old_scenefx_versions()
    elif package == "xdg-desktop-portal-hyprland-git":
        # Build steps
        cmake_configure = "cmake -DCMAKE_INSTALL_LIBEXECDIR=/usr/lib -DCMAKE_INSTALL_PREFIX=/usr -B build"
        cmake_build = "cmake --build build"
        cmake_install = "sudo cmake --install build"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)
    elif package == "libdisplay-info-git":
        # Build steps
        cmake_configure = "PKG_CONFIG_PATH=/usr/lib/pkgconfig meson setup build/ --prefix=/usr --buildtype=release"
        cmake_build = "ninja -C build/"
        cmake_install = "sudo ninja -C build/ install"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)

        # Cleanup old versions
        cleanup_old_display_info_versions()
    # -- Binaries --
    elif package == "hypridle-git":

        # wayland
        # wayland-protocols
        # hyprlang >= 0.4.0
        # sdbus-c++

        # Build steps
        cmake_configure = "cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_INSTALL_PREFIX:PATH=/usr -S . -B ./build"
        cmake_build = "cmake --build ./build --config Release --target hypridle -j$(nproc 2>/dev/null || getconf _NPROCESSORS_CONF)"
        cmake_install = "sudo cmake --install build"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)
    elif package == "hyprlock-git":
        # Build steps
        cmake_configure = "cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_INSTALL_PREFIX:PATH=/usr -S . -B ./build"
        cmake_build = "cmake --build ./build --config Release --target hyprlock -j$(nproc 2>/dev/null || getconf _NPROCESSORS_CONF)"
        cmake_install = "sudo cmake --install build"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)
    elif package == "hyprshot-git":
        # copy the binary to /usr/bin
        run_command(f"sudo cp {package_dir}/hyprshot /usr/bin")
        # Make the script executable
        run_command(f"sudo chmod +x /usr/bin/hyprshot")
    elif package == "hyprwayland-scanner-git":
        # Build steps
        cmake_configure = "cmake -DCMAKE_INSTALL_PREFIX=/usr -B build"
        cmake_build = "cmake --build ./build -j$(nproc)"
        cmake_install = "sudo cmake --install build"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)
    elif package == "wayvnc-git":
        neatvnc_repo = "https://github.com/any1/neatvnc.git"
        aml_repo = "https://github.com/any1/aml.git"

        neatvnc_dir = package_dir / "neatvnc"
        aml_dir = package_dir / "aml"

        if not neatvnc_dir.exists():
            run_command(f"git clone {neatvnc_repo}", cwd=package_dir)
        if not aml_dir.exists():
            run_command(f"git clone {aml_repo}", cwd=package_dir)

        # Step 2: Set up subproject links
        wayvnc_subprojects = package_dir / "subprojects"
        neatvnc_subprojects = neatvnc_dir / "subprojects"

        wayvnc_subprojects.mkdir(exist_ok=True)
        neatvnc_subprojects.mkdir(exist_ok=True)

        # Link neatvnc and aml to wayvnc subprojects
        (wayvnc_subprojects / "neatvnc").symlink_to(neatvnc_dir.resolve())
        (wayvnc_subprojects / "aml").symlink_to(aml_dir.resolve())

        # Link aml to neatvnc subprojects
        (neatvnc_subprojects / "aml").symlink_to(aml_dir.resolve())

        # Build steps
        cmake_configure = "meson build --prefix=/usr"
        cmake_build = "ninja -C build"
        cmake_install = "sudo ninja -C build install"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)
    elif package == "waybar-git":
        # Build steps
        cmake_configure = "PKG_CONFIG_PATH=/usr/lib/pkgconfig meson setup build/ --prefix=/usr -Dtests=disabled"
        cmake_build = "ninja -C build"
        cmake_install = "sudo ninja -C build install"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)
    elif package == "wl-clipboard-git":
        # Build steps
        cmake_configure = (
            "PKG_CONFIG_PATH=/usr/lib/pkgconfig meson setup build/ --prefix=/usr"
        )
        cmake_build = "ninja -C build"
        cmake_install = "sudo ninja -C build install"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)
    elif (
        package == "wlogout-git"
        or package == "wlr-randr-git"
        or package == "swaybg-git"
    ):
        # Build steps
        cmake_configure = (
            "PKG_CONFIG_PATH=/usr/lib/pkgconfig meson setup build/ --prefix=/usr"
        )
        cmake_build = "ninja -C build"
        cmake_install = "sudo ninja -C build install"

        run_command(cmake_configure, cwd=package_dir)
        run_command(cmake_build, cwd=package_dir)
        run_command(cmake_install, cwd=package_dir)
    # -- Themes --
    elif package == "kora-icon-theme":
        """Install each specified icon theme folder to /usr/share/icons."""
        themes = ["kora", "kora-light", "kora-light-panel", "kora-pgrey"]
        for theme in themes:
            theme_src_dir = package_dir / theme
            target_dir = Path("/usr/share/icons") / theme

            if not theme_src_dir.exists():
                print(
                    f"Warning: Theme directory {theme_src_dir} does not exist. Skipping."
                )
                continue

            print(f"Installing {theme} icon theme to {target_dir}...")

            # Create target directory if it doesn't exist
            run_command(f"sudo mkdir -p {target_dir}")

            # Copy the theme to /usr/share/icons
            run_command(f"sudo rsync -a --delete {theme_src_dir}/ {target_dir}/")

            # Update the icon cache for this theme
            print(f"Updating icon cache for {theme}...")
            run_command(f"sudo gtk-update-icon-cache {target_dir}")
    elif package == "nerd-fonts-git":
        """Install the Nerd Fonts to /usr/share/fonts."""
        install_script = package_dir / "install.sh"

        if not install_script.exists():
            print(f"Error: install.sh not found in {package_dir}.")
            return

        run_command(f"bash {install_script}", cwd=package_dir)

        # Update the font cache
        run_command("sudo fc-cache -f -v")
    elif package == "otf-openmoji":
        """Install the openmoji to /usr/share/fonts."""
        font_dir = Path("/usr/share/fonts/OpenMoji")

        # Create the target directory if it doesn't exist
        print(f"Creating target directory at {font_dir}...")
        run_command(f"sudo mkdir -p {font_dir}")

        fonts = [
            "font/OpenMoji-black-glyf/OpenMoji-black-glyf.ttf",
            "font/OpenMoji-color-colr0_svg/OpenMoji-color-colr0_svg.ttf",
        ]
        for font in fonts:
            font_path = package_dir / font
            if not font_path.exists():
                print(f"Warning: Font file {font_path} does not exist. Skipping.")
            continue

        print(f"Installing {font_path.name} to {font_dir}...")
        run_command(f"sudo cp {font_path} {font_dir}/")
    elif package == "mesa-git":
        # Build steps
        setup = "meson setup build"
        build = "ninja -C build"
        install = "sudo ninja -C build/ install"

        run_command(setup, cwd=package_dir)
        run_command(build, cwd=package_dir)
        run_command(install, cwd=package_dir)
    elif package == "hyprgraphics-git":
        # Build steps
        setup = "cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_INSTALL_PREFIX:PATH=/usr -S . -B ./build"
        build = "cmake --build ./build --config Release --target all -j`nproc 2>/dev/null || getconf NPROCESSORS_CONF`"
        install = "sudo cmake --install build"

        run_command(setup, cwd=package_dir)
        run_command(build, cwd=package_dir)
        run_command(install, cwd=package_dir)

    #
    print(f"{package} built successfully.")


# def clean_repo(package_dir):
#     """Remove the cloned repository after building."""
#     if package_dir.exists():
#         print(f"Cleaning up {package_dir}...")
#         run_command(f"rm -rf {package_dir}")
#         print(f"{package_dir} removed.")


def reset_repo(package_dir):
    """Reset a Git repository to a clean state."""
    if not package_dir.exists():
        print(f"The directory {package_dir} does not exist.")
        return

    print(f"Resetting the Git repository in {package_dir}...")

    try:
        # Ensure the directory is a Git repository
        subprocess.run(
            ["git", "-C", str(package_dir), "rev-parse", "--is-inside-work-tree"],
            check=True,
            capture_output=True,
        )

        # Clean untracked files and reset to the latest commit
        subprocess.run(["git", "-C", str(package_dir), "reset", "--hard"], check=True)
        subprocess.run(["git", "-C", str(package_dir), "clean", "-fd"], check=True)

        # Optionally pull the latest changes
        subprocess.run(["git", "-C", str(package_dir), "pull"], check=True)

        print(f"Repository at {package_dir} has been reset to a clean state.")
    except subprocess.CalledProcessError as e:
        print(f"Failed to reset the repository: {e}")


def main():
    # check dependencies
    # check_dependencies()

    # get_latest_mesa_version()

    # if BUILD_DIR.exists():
    #     print(f"Cleaning up {BUILD_DIR}...")
    #     run_command(f"rm -rf {BUILD_DIR}")
    #     print(f"{BUILD_DIR} removed.")

    BUILD_DIR.mkdir(parents=True, exist_ok=True)

    file_path = BUILD_DIR / "persistent_dict.json"

    # Create an instance of PersistentDictionary
    my_dict = settings.PersistentDictionary(file_path)

    for package, repo_url in PACKAGES.items():
        print(f"\nProcessing {package}...")
        # Step 1: Clone the repository
        package_dir = clone_repo(package, repo_url)

        # Step 2: Check the last commit hash
        last_commit = None
        try:
            # Get the last commit hash from the repository
            last_commit = subprocess.check_output(
                ["git", "-C", str(package_dir), "rev-parse", "HEAD"], text=True
            ).strip()
        except subprocess.CalledProcessError as e:
            print(f"Error getting last commit hash for {package}: {e}")
            continue  # Skip this package if unable to get the commit hash

        # Compare with the saved hash in my_dict
        saved_hash = my_dict.get(package)
        if saved_hash == last_commit:
            print(f"{package} is up-to-date. Skipping build.")
            continue

        # Step 3: Build the package
        build_package(package, package_dir)

        # Step 4: Clean up the cloned repository
        # clean_repo(package_dir)
        reset_repo(package_dir)

        # Step 5: Update the saved hash in my_dict
        my_dict.set(package, last_commit)

    # Final Step: Update the dynamic linker cache
    print("Updating the dynamic linker cache...")
    run_command("sudo ldconfig")

    print("All packages processed.")


if __name__ == "__main__":
    main()
