import requests
import platform
import subprocess
from pathlib import Path
import tempfile


def get_latest_kitty_version():
    """Fetch the latest Kitty version from the GitHub API."""
    github_api_url = "https://api.github.com/repos/kovidgoyal/kitty/releases/latest"
    try:
        response = requests.get(github_api_url)
        response.raise_for_status()
        latest_release = response.json()
        return latest_release["tag_name"].lstrip(
            "v"
        )  # Remove the "v" prefix from the version
    except requests.RequestException as e:
        print(f"Error fetching Kitty version from GitHub: {e}")
        return None


def get_installed_version(terminal_name):
    """Get the installed version of a terminal."""
    try:
        result = subprocess.run(
            [terminal_name, "--version"], capture_output=True, text=True
        )
        if result.returncode == 0:
            version = result.stdout.strip().split()[
                1
            ]  # Assuming version is the second word
            return version
    except FileNotFoundError:
        return None
    return None


def setup_kitty_launcher(binary_path, icon_path, desktop_file_path):
    """Set up the Kitty application launcher."""
    desktop_file = Path(desktop_file_path)

    if desktop_file.exists():
        print(f"Desktop file already exists at {desktop_file}. Skipping creation.")
        return

    print("Setting up Kitty application launcher...")

    desktop_entry = f"""
    [Desktop Entry]
    Name=Kitty
    Exec={str(binary_path)}
    Icon={str(icon_path)}
    Terminal=false
    Type=Application
    Categories=Utility;TerminalEmulator;
    """

    try:
        # Create a temporary file in a writable directory
        with tempfile.NamedTemporaryFile("w", delete=False) as temp_file:
            temp_file_path = temp_file.name
            temp_file.write(desktop_entry.strip())

        # Move the temporary file to the destination
        subprocess.run(["sudo", "mv", temp_file_path, desktop_file_path], check=True)
        subprocess.run(["sudo", "chmod", "644", desktop_file_path], check=True)

        print("Kitty application launcher created successfully.")
    except Exception as e:
        print(f"Error setting up Kitty application launcher: {e}")


def install_alacritty():
    """Install Alacritty from source based on its official instructions."""
    github_repo = "alacritty/alacritty"
    github_api_url = f"https://api.github.com/repos/{github_repo}/releases/latest"

    try:
        # Fetch the latest release information
        response = requests.get(github_api_url)
        response.raise_for_status()
        latest_release = response.json()
        latest_version = latest_release["tag_name"].strip("v")

        # Check if Alacritty is already installed and up-to-date
        installed_version = get_installed_version("alacritty")
        if installed_version:
            if installed_version == latest_version:
                print(f"Alacritty is already up-to-date (version {installed_version}).")
                return
            else:
                print(
                    f"Updating Alacritty from version {installed_version} to {latest_version}..."
                )
        else:
            print(f"Alacritty is not installed. Installing version {latest_version}...")

        # Clone the repository and build from source
        with tempfile.TemporaryDirectory() as temp_dir:
            alacritty_src_path = Path(temp_dir) / "alacritty"

            # Clone the repository
            subprocess.run(
                [
                    "git",
                    "clone",
                    f"https://github.com/{github_repo}.git",
                    str(alacritty_src_path),
                ],
                check=True,
            )
            # Check out the latest tag
            subprocess.run(
                ["git", "checkout", f"v{latest_version}"],
                cwd=str(alacritty_src_path),
                check=True,
            )

            # Build Alacritty using Cargo
            subprocess.run(
                ["cargo", "build", "--release"], cwd=str(alacritty_src_path), check=True
            )

            # Install the binary
            alacritty_bin = alacritty_src_path / "target/release/alacritty"
            if not alacritty_bin.exists():
                print("Failed to build Alacritty binary.")
                return

            # Move the binary to /usr/bin
            subprocess.run(
                ["sudo", "mv", str(alacritty_bin), "/usr/bin/alacritty"], check=True
            )
            subprocess.run(["sudo", "chmod", "+x", "/usr/bin/alacritty"], check=True)

            # Install resources directly from the source `extra` folder
            extra_path = alacritty_src_path / "extra"

            # Set up desktop entry and logo
            subprocess.run(
                [
                    "sudo",
                    "cp",
                    str(extra_path / "logo/alacritty-term.svg"),
                    "/usr/share/pixmaps/",
                ],
                check=True,
            )
            subprocess.run(
                [
                    "sudo",
                    "desktop-file-install",
                    str(extra_path / "linux/Alacritty.desktop"),
                ],
                check=True,
            )
            subprocess.run(["sudo", "update-desktop-database"], check=True)

        print(f"Alacritty {latest_version} installed successfully.")
    except requests.RequestException as e:
        print(f"Failed to fetch the latest release information: {e}")
    except subprocess.CalledProcessError as e:
        print(f"Error during Alacritty installation: {e}")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")


def install_kitty():
    # Define paths and URLs
    binary_install_url = "https://sw.kovidgoyal.net/kitty/installer.sh"
    install_dir = Path("/opt")
    binary_path = Path("/usr/bin/kitty")
    icon_path = "/usr/share/icons/kitty.png"
    desktop_file_path = "/usr/share/applications/kitty.desktop"

    try:
        # Get installed version
        installed_version = get_installed_version("kitty")
        print(f"Installed Kitty version: {installed_version}")

        # Get latest version
        latest_version = get_latest_kitty_version()
        if not latest_version:
            print(
                "Could not determine the latest Kitty version. Aborting installation."
            )
            return

        print(f"Latest Kitty version: {latest_version}")

        # Compare versions
        if installed_version == latest_version:
            print("Kitty is up-to-date. No installation needed.")
            return
        elif installed_version:
            print(
                f"Updating Kitty from version {installed_version} to {latest_version}..."
            )

        # Download and run the installer script
        print("Downloading and running Kitty installer script...")
        install_script_path = "/tmp/kitty_installer.sh"
        with requests.get(binary_install_url, stream=True) as response:
            response.raise_for_status()
            with open(install_script_path, "wb") as script_file:
                for chunk in response.iter_content(chunk_size=8192):
                    script_file.write(chunk)

        # Run the installer script
        subprocess.run(
            ["sudo", "sh", install_script_path, "dest=" + str(install_dir)], check=True
        )

        # Ensure binary is linked to /usr/bin
        print("Creating symlink to Kitty binary...")
        kitty_executable = install_dir / "kitty.app" / "bin" / "kitty"
        subprocess.run(
            ["sudo", "ln", "-sf", str(kitty_executable), str(binary_path)], check=True
        )

        # Install the icon
        print("Installing Kitty icon...")
        kitty_icon = (
            install_dir
            / "kitty.app"
            / "share"
            / "icons"
            / "hicolor"
            / "256x256"
            / "apps"
            / "kitty.png"
        )
        # kitty_icon = (
        #     Path.home()
        #     / ".local"
        #     / "kitty.app"
        #     / "share"
        #     / "icons"
        #     / "hicolor"
        #     / "256x256"
        #     / "apps"
        #     / "kitty.png"
        # )
        subprocess.run(["sudo", "cp", str(kitty_icon), icon_path], check=True)

        # Set up application launcher
        print("Setting up Kitty application launcher...")
        setup_kitty_launcher(binary_path, icon_path, desktop_file_path)

        print("Kitty installation complete.")
    except requests.RequestException as e:
        print("Failed to download Kitty installer script:", e)
    except subprocess.CalledProcessError as e:
        print("Error during installation:", e)
    except Exception as e:
        print("An unexpected error occurred:", e)


def main():
    os_release = platform.system()
    # if os_release == "Darwin":
    #     print("MacOS detected. Running updates with brew.")

    if os_release != "Linux":
        print("Not running on a Linux system. Exiting.")
        return

    try:
        with open("/etc/os-release") as f:
            os_info = f.read()
    except FileNotFoundError:
        print("Unable to verify OS. Exiting.")
        return

    is_ubuntu = "Ubuntu" in os_info
    # is_arch = "Arch Linux" in os_info

    if is_ubuntu:
        # install ghostty on ubuntu systems.
        subprocess.run(
            [
                "/bin/bash",
                "-c",
                "$(curl -fsSL https://raw.githubusercontent.com/mkasberg/ghostty-ubuntu/HEAD/install.sh)",
            ],
            check=True,
        )

    print("Installing Alacritty...")
    install_alacritty()
    print("\nInstalling Kitty...")
    install_kitty()


if __name__ == "__main__":
    main()
