# rust.py is a part of the bootstrap process for the Rust setup.
# handles installing, update of rustup and cargo.
# handles installing of rust toolchain, components and targets along with cleanup.

import os
import subprocess
import shutil
from pathlib import Path


def rust_install():
    if not shutil.which("rustc"):
        print("Installing Rust...")
        subprocess.run(
            ["curl", "--proto", "=https", "--tlsv1.2", "-sSf", "https://sh.rustup.rs"],
            stdout=subprocess.PIPE,
            check=True,
        )
        subprocess.run(["sh"], input=subprocess.PIPE, check=True)


def rust_update():
    if shutil.which("rustc"):
        print("Rust is already installed.")
        subprocess.run(["rustup", "update"], check=True)
        subprocess.run(["rustup", "toolchain", "install", "stable"], check=True)
        subprocess.run(["rustup", "component", "add", "rustfmt"], check=True)
    else:
        rust_install()


def rust_tools_install():
    tools = [
        "--branch main --git https://github.com/Kampfkarren/selene selene",
        "git-delta",
        "eza",
        "fd-find",
        "bat",
        "ripgrep",
        "cargo-update",
        "cargo-cache",
        "starship",
        "stylua",
        "tlrc",
        "zellij",
        "zoxide",
    ]

    if shutil.which("rustc"):
        print("Rust is already installed.")
        subprocess.run(["rustup", "update"], check=True)

        for tool in tools:
            print(f"Installing {tool}...")
            subprocess.run(["cargo", "install"] + tool.split(), check=True)

        # Update installed tools
        subprocess.run(["cargo", "install-update", "-a"], check=True)
        # Clear the cache from cargo installs
        subprocess.run(["cargo", "cache", "-a"], check=True)


def rust_setup():
    cargo_bin_path = Path.home() / ".cargo/bin"
    if cargo_bin_path.is_dir():
        os.environ["PATH"] = f"{cargo_bin_path}:{os.environ['PATH']}"
        print(
            subprocess.run(
                ["rustc", "-V"], capture_output=True, text=True
            ).stdout.strip()
        )
        os.environ["RUST_BACKTRACE"] = "1"


def rust_cleanup():
    current_dir = Path.cwd()
    projects_dir = Path.home() / "Projects"

    for cargo_file in projects_dir.rglob("Cargo.toml"):
        dir_path = cargo_file.parent
        print(f"Cleaning up Rust project in: {dir_path}")
        subprocess.run(["cargo", "clean"], cwd=dir_path, check=True)

    os.chdir(current_dir)


def main():
    rust_setup()
    rust_update()
    rust_tools_install()
    rust_cleanup()


if __name__ == "__main__":
    main()
