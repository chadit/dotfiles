# system contains common applications that are used by various other step builds.
# opting to install from sources instead of using package manager, because the package manager may not have the latest version.

# The following applications are installed:
# - meson (build system)
# - ninja (build system)
# - neovim (text editor)

import requests
import subprocess
import os
import re
import tarfile
import tempfile
from pathlib import Path
import neovim
import vscode
import platform
import terminal
import rust

# import hyprland
# import sway

# :: removing ninja breaks dependency 'ninja' required by python-cmake-build-extension
# :: removing ninja breaks dependency 'ninja' required by vcpkg


# TODO Dependencies
# Arch
# - sudo pacman -S python-requests


# Ubuntu
# pip install requests


def extract_filter(tarinfo, tarfile):
    # Customize this function based on your needs
    return tarinfo  # Return tarinfo unmodified to allow all files


# Meson Build System Installation
def get_installed_meson_version():
    try:
        result = subprocess.run(
            ["meson", "--version"], stdout=subprocess.PIPE, text=True, check=True
        )
        return result.stdout.strip()
    except FileNotFoundError:
        return None


def download_and_install_latest_meson():
    installed_version = get_installed_meson_version()
    print(f"Currently installed Meson version: {installed_version or 'None'}")

    # Fetch the latest release info from GitHub API
    api_url = "https://api.github.com/repos/mesonbuild/meson/releases/latest"
    response = requests.get(api_url)
    if response.status_code != 200:
        print("Failed to fetch the latest release information.")
        return

    release_info = response.json()
    latest_version = release_info.get("tag_name").lstrip("v")
    print(f"Latest Meson version available: {latest_version}")

    # Check if the latest version is already installed
    if installed_version == latest_version:
        print("Meson is already up-to-date.")
        return
    else:
        print("Updating to the latest Meson version...")

    tarball_url = None

    # Find the .tar.gz asset in the release assets
    for asset in release_info.get("assets", []):
        if asset["name"].endswith(".tar.gz"):
            tarball_url = asset["browser_download_url"]
            break

    if not tarball_url:
        print("Could not find a .tar.gz asset in the latest release.")
        return

    print(f"Found latest Meson tar.gz URL: {tarball_url}")

    # Download the tar.gz file
    response = requests.get(tarball_url, stream=True)
    if response.status_code != 200:
        print("Failed to download the Meson tar.gz file.")
        return

    with tempfile.NamedTemporaryFile(delete=False, suffix=".tar.gz") as tmp_file:
        for chunk in response.iter_content(chunk_size=8192):
            tmp_file.write(chunk)
        tmp_file_path = tmp_file.name

    # Extract and install Meson
    with tempfile.TemporaryDirectory() as temp_dir:
        with tarfile.open(tmp_file_path) as tar:
            tar.extractall(temp_dir, filter=extract_filter)

        # Locate the meson.py file in the extracted folder
        meson_folder = None
        for root, dirs, files in os.walk(temp_dir):
            if "meson.py" in files:
                meson_folder = root
                break

        if not meson_folder:
            print("Meson folder with meson.py script not found in extracted files.")
            return

        # Define the installation path
        install_dir = "/usr/meson"
        install_script = "/usr/bin/meson"

        # Use 'sudo' to copy the entire directory and set up the symlink
        print(f"Installing Meson to {install_dir}")

        # Remove old installation if exists
        subprocess.run(["sudo", "rm", "-rf", install_dir], check=True)
        subprocess.run(["sudo", "cp", "-r", meson_folder, install_dir], check=True)
        subprocess.run(
            [
                "sudo",
                "ln",
                "-sf",
                os.path.join(install_dir, "meson.py"),
                install_script,
            ],
            check=True,
        )
        subprocess.run(["sudo", "chmod", "+x", install_script], check=True)

    # Clean up downloaded file
    os.remove(tmp_file_path)
    print("Meson installation complete.")


def install_latest_ninja():
    github_api_url = "https://api.github.com/repos/ninja-build/ninja/releases/latest"
    local_ninja_path = Path("/usr/bin/ninja")

    try:
        # Get the latest version from GitHub
        response = requests.get(github_api_url)
        response.raise_for_status()
        latest_release = response.json()
        latest_version = latest_release["tag_name"].strip("v")

        # Check if Ninja is already installed and compare versions
        if local_ninja_path.exists():
            current_version_output = subprocess.run(
                ["ninja", "--version"], capture_output=True, text=True
            )
            current_version = current_version_output.stdout.strip()

            # Compare versions
            if current_version >= latest_version:
                print(f"Ninja is up-to-date (version {current_version}).")
                return
            print(
                f"Updating Ninja from version {current_version} to {latest_version}..."
            )

        # Determine system architecture
        arch = subprocess.run(
            ["uname", "-m"], capture_output=True, text=True
        ).stdout.strip()

        print(f"System architecture: {arch}")

        # Set download URL for x86_64
        if arch == "x86_64":
            asset_url = next(
                (
                    asset["browser_download_url"]
                    for asset in latest_release["assets"]
                    if asset["name"] == "ninja-linux.zip"
                ),
                None,
            )
        else:
            # Match other architecture-specific patterns if not x86_64
            arch_pattern = r"linux.*aarch64.*\.zip$" if arch == "aarch64" else None
            asset_url = next(
                (
                    asset["browser_download_url"]
                    for asset in latest_release["assets"]
                    if arch_pattern and re.search(arch_pattern, asset["name"])
                ),
                None,
            )

        if not asset_url:
            print(f"No compatible Ninja binary found for architecture: {arch}")
            return

        # Download and extract the zip file
        with tempfile.TemporaryDirectory() as temp_dir:
            zip_path = os.path.join(temp_dir, "ninja.zip")
            with requests.get(asset_url, stream=True) as download:
                download.raise_for_status()
                with open(zip_path, "wb") as file:
                    for chunk in download.iter_content(chunk_size=8192):
                        file.write(chunk)

            # Extract the binary
            subprocess.run(["unzip", "-o", zip_path, "-d", temp_dir], check=True)
            ninja_binary = Path(temp_dir) / "ninja"

            # Validate the binary
            file_info = subprocess.run(
                ["file", str(ninja_binary)], capture_output=True, text=True
            ).stdout.strip()
            print(f"File info: {file_info}")

            arch_map = {"x86_64": "x86-64", "aarch64": "ARM aarch64"}

            # Default to arch if not in map
            normalized_arch = arch_map.get(arch, arch)

            print(f"Expected arch in file info: {normalized_arch}")

            if "ELF" not in file_info or normalized_arch not in file_info:
                print(
                    f"The downloaded Ninja binary is not compatible with {arch}. Installation aborted."
                )
                return

            # Move the new ninja binary to /usr/bin
            subprocess.run(["sudo", "cp", ninja_binary, "/usr/bin/ninja"], check=True)
            subprocess.run(["sudo", "chmod", "+x", "/usr/bin/ninja"], check=True)
            print(f"Ninja {latest_version} has been installed in /usr/bin.")

    except requests.RequestException as e:
        print("Failed to connect to GitHub:", e)
    except subprocess.CalledProcessError as e:
        print("Failed to install Ninja:", e)
    except Exception as e:
        print("An error occurred:", e)


def system_macos_update():
    try:
        subprocess.run(["brew", "update"], check=True)
        subprocess.run(["brew", "upgrade"], check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error while updating MacOS: {e}")


def system_update():
    # Check if the OS is Ubuntu
    os_release = platform.system()
    if os_release == "Darwin":
        print("MacOS detected. Running updates with brew.")
        system_macos_update()
        return

    if os_release != "Linux":
        print("Not running on a Linux system. Exiting.")
        return

    try:
        with open("/etc/os-release") as f:
            os_info = f.read()
    except FileNotFoundError:
        print("Unable to verify OS. Exiting.")
        return

    is_ubuntu = "Ubuntu" in os_info
    is_arch = "Arch Linux" in os_info

    if is_arch:
        print("Arch Linux detected. Running updates with yay.")
        try:
            subprocess.run(
                [
                    "yay",
                    "-Syyu",
                    "--noconfirm",
                ],
                check=True,
            )
        except subprocess.CalledProcessError as e:
            print(f"Error while updating Arch: {e}")
        return

    if is_ubuntu:
        print("Ubuntu detected. Running updates with apt.")
        try:
            subprocess.run(["sudo", "apt", "update"], check=True)
            subprocess.run(["sudo", "apt", "upgrade", "-y"], check=True)
        except subprocess.CalledProcessError as e:
            print(f"Error while updating Ubuntu: {e}")
        return


def desktop_environment():
    os_release = platform.system()
    if os_release != "Linux":
        print("Not running on a Linux system. Exiting.")
        return

    try:
        with open("/etc/os-release") as f:
            os_info = f.read()
    except FileNotFoundError:
        print("Unable to verify OS. Exiting.")
        return

    is_ubuntu = "Ubuntu" in os_info
    # is_arch = "Arch Linux" in os_info

    # currently only supported on Arch Linux
    # if is_arch:
    # hyprland.main()

    if is_ubuntu:
        neovim.install_neovim()

    # install on all systems
    # sway.main()


def main():
    # download_and_install_latest_meson()
    # install_latest_ninja()
    # neovim.install_neovim() using yay to install on arch
    vscode.main()
    terminal.main()
    # desktop_environment()
    system_update()
    rust.main()


if __name__ == "__main__":
    main()
