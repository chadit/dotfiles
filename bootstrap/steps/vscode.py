import requests
import subprocess
import platform
import os
from pathlib import Path
import tarfile


def extract_filter(tarinfo, tarfile):
    # Customize this function based on your needs
    return tarinfo  # Return tarinfo unmodified to allow all files


def install_vscode(edition="stable"):
    """
    Installs Visual Studio Code or Visual Studio Code Insider edition.
    :param edition: 'stable' or 'insider'
    """
    # Set the URL and installation paths
    if edition == "stable":
        url = "https://update.code.visualstudio.com/latest/linux-x64/stable"
        install_dir = Path("/opt/vscode")
        binary_name = "code"
        binary_exec = install_dir / "VSCode-linux-x64" / "bin" / binary_name
        desktop_file = "/usr/share/applications/code.desktop"
    elif edition == "insider":
        url = "https://update.code.visualstudio.com/latest/linux-x64/insider"
        install_dir = Path("/opt/vscode-insiders")
        binary_name = "code-insiders"
        binary_exec = install_dir / "VSCode-linux-x64" / "bin" / binary_name
        desktop_file = "/usr/share/applications/code-insiders.desktop"
    else:
        print("Invalid edition specified. Use 'stable' or 'insider'.")
        return

    # Check if the latest version is already installed
    # if install_dir.exists() and binary_exec.exists():
    #     current_version_output = subprocess.run(
    #         [str(binary_exec), "--version"], capture_output=True, text=True
    #     )
    #     current_version = current_version_output.stdout.strip().splitlines()[0]
    #     print(f"{binary_name} is already installed (version {current_version}).")
    #     return

    print(f"Downloading {binary_name} from {url}...")

    # Download the tarball
    response = requests.get(url, stream=True)
    response.raise_for_status()
    tarball_path = Path(f"/tmp/{binary_name}.tar.gz")
    with open(tarball_path, "wb") as tarball:
        for chunk in response.iter_content(chunk_size=8192):
            tarball.write(chunk)

    print(f"Extracting {binary_name} to {install_dir}...")

    # Extract the tarball
    with tarfile.open(tarball_path, "r:gz") as tar:
        if install_dir.exists():
            subprocess.run(["sudo", "rm", "-rf", str(install_dir)])
        subprocess.run(["sudo", "mkdir", "-p", str(install_dir)])
        subprocess.run(
            ["sudo", "chown", f"{os.getuid()}:{os.getgid()}", str(install_dir)]
        )
        tar.extractall(path=install_dir, filter=extract_filter)

    # Cleanup tarball after extraction
    print(f"Cleaning up tarball {tarball_path}...")
    tarball_path.unlink()

    # Create a symlink in /usr/bin
    subprocess.run(["sudo", "ln", "-sf", str(binary_exec), f"/usr/bin/{binary_name}"])

    print(f"Setting up application shortcut for {binary_name}...")

    # Create a temporary .desktop file for the application
    desktop_entry = f"""
    [Desktop Entry]
    Name=Visual Studio Code {edition.title()}
    Comment=Code Editing. Redefined.
    Exec=/usr/bin/{binary_name} %F
    Icon={install_dir}/VSCode-linux-x64/resources/app/resources/linux/code.png
    Terminal=false
    Type=Application
    Categories=Development;IDE;
    MimeType=text/plain;inode/directory;
    """
    temp_desktop_file = Path(f"/tmp/{binary_name}.desktop")
    with open(temp_desktop_file, "w") as desktop:
        desktop.write(desktop_entry.strip())

    # Move the .desktop file to /usr/share/applications with sudo
    subprocess.run(["sudo", "mv", str(temp_desktop_file), desktop_file], check=True)
    subprocess.run(["sudo", "chmod", "+x", desktop_file], check=True)

    print(f"{binary_name} installation complete!")


def main():
    os_release = platform.system()
    if os_release != "Linux":
        print("Not running on a Linux system. Exiting.")
        return

    # Install Visual Studio Code and Visual Studio Code Insiders
    install_vscode("stable")
    install_vscode("insider")


if __name__ == "__main__":
    main()
