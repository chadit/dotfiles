import os


def main():
    print("Disabling GNOME animations...")
    os.system("gsettings set org.gnome.desktop.interface enable-animations false")
    # print("Disabling GNOME remote display...")
    # os.system(
    #     "gsettings set org.gnome.settings-daemon.plugins.remote-display active false"
    # )
    print("Done.")


if __name__ == "__main__":
    main()
