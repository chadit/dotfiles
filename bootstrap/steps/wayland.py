import shutil
import subprocess
import os
import sys
from pathlib import Path

WAYLAND_REPO_URL = "https://gitlab.freedesktop.org/wayland/wayland.git"
WAYLAND_PROTOCOLS_REPO_URL = (
    "https://gitlab.freedesktop.org/wayland/wayland-protocols.git"
)
LOCAL_WAYLAND_PATH = Path.home() / "tmp/wayland"  # Use ~/tmp/wayland
LOCAL_WAYLAND_PROTOCOLS_PATH = (
    Path.home() / "tmp/wayland-protocols"
)  # Use ~/tmp/wayland-protocols
BUILD_PATH = LOCAL_WAYLAND_PATH / "build"
PROTOCOLS_BUILD_PATH = LOCAL_WAYLAND_PROTOCOLS_PATH / "build"


def run_command(command, cwd=None):
    """Run a shell command and capture both stdout and stderr."""
    result = subprocess.run(
        command,
        cwd=cwd,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        text=True,
    )
    if result.returncode != 0:
        print(f"Error running command: {command}")
        print("Output/Error:", result.stdout)
        sys.exit(1)
    return result.stdout.strip()


def check_installed_version():
    """Check the currently installed Wayland version."""
    wayland_scanner_path = shutil.which("wayland-scanner")

    if wayland_scanner_path is None:
        print("Error: wayland-scanner not found in PATH.")
        return None

    print(f"Found wayland-scanner at: {wayland_scanner_path}")

    try:
        # Run the command and print the output directly for debugging
        version_output = run_command(f"{wayland_scanner_path} --version")
        print(f"Raw output from wayland-scanner --version: '{version_output}'")

        if version_output:
            # Extract only the version number, assuming format: "wayland-scanner X.Y.Z"
            parts = version_output.split()
            if len(parts) >= 2:
                version = parts[1]
                return version
            else:
                print("Unexpected version format:", version_output)
                return None
        else:
            print("No output from wayland-scanner --version")
            return None
    except Exception as e:
        print(f"Error checking installed version: {e}")
        return None


def get_latest_version():
    """Fetch the latest release tag from the Wayland Git repository."""
    if LOCAL_WAYLAND_PATH.exists():
        run_command("git fetch --tags", cwd=LOCAL_WAYLAND_PATH)
    else:
        print(f"Creating directory {LOCAL_WAYLAND_PATH}")
        LOCAL_WAYLAND_PATH.mkdir(parents=True, exist_ok=True)
        run_command(f"git clone {WAYLAND_REPO_URL} {LOCAL_WAYLAND_PATH}")

    # Get the latest tag
    latest_tag = run_command(
        "git describe --tags $(git rev-list --tags --max-count=1)",
        cwd=LOCAL_WAYLAND_PATH,
    )
    print(f"Latest release version (tag): {latest_tag}")
    return latest_tag


def build_and_install():
    """Build and install the latest Wayland version from source."""
    # Setup the build directory if it doesn't exist
    if not BUILD_PATH.exists():
        setup_result = run_command(
            "meson setup build --prefix=/usr", cwd=LOCAL_WAYLAND_PATH
        )
        print("Meson setup output:", setup_result)

    # Verify that the build.ninja file exists before proceeding
    build_ninja_path = BUILD_PATH / "build.ninja"
    if not build_ninja_path.exists():
        print("Error: 'build.ninja' file not found. Meson setup may have failed.")
        sys.exit(1)

    # Build
    run_command("ninja", cwd=BUILD_PATH)

    # Install with sudo
    run_command("sudo ninja install", cwd=BUILD_PATH)
    print("Wayland updated successfully to /usr.")


def build_and_install_wayland_protocols():
    """Build and install wayland-protocols from source."""
    if not LOCAL_WAYLAND_PROTOCOLS_PATH.exists():
        print(f"Creating directory {LOCAL_WAYLAND_PROTOCOLS_PATH}")
        LOCAL_WAYLAND_PROTOCOLS_PATH.mkdir(parents=True, exist_ok=True)
        run_command(
            f"git clone {WAYLAND_PROTOCOLS_REPO_URL} {LOCAL_WAYLAND_PROTOCOLS_PATH}"
        )

    # Setup the meson build directory and install
    if not PROTOCOLS_BUILD_PATH.exists():
        run_command("meson setup build --prefix=/usr", cwd=LOCAL_WAYLAND_PROTOCOLS_PATH)

    # Build and install wayland-protocols
    run_command("ninja", cwd=PROTOCOLS_BUILD_PATH)
    run_command("sudo ninja install", cwd=PROTOCOLS_BUILD_PATH)
    print("wayland-protocols installed successfully to /usr.")


def cleanup():
    """Remove the local build directories after installation."""
    if LOCAL_WAYLAND_PATH.exists():
        print(f"Cleaning up directory {LOCAL_WAYLAND_PATH}")
        shutil.rmtree(LOCAL_WAYLAND_PATH)
    if LOCAL_WAYLAND_PROTOCOLS_PATH.exists():
        print(f"Cleaning up directory {LOCAL_WAYLAND_PROTOCOLS_PATH}")
        shutil.rmtree(LOCAL_WAYLAND_PROTOCOLS_PATH)


def main():
    print("Checking for Wayland updates...")

    installed_version = check_installed_version()
    if not installed_version:
        print("Error: Could not determine installed Wayland version.")
    # sys.exit(1)

    latest_version = get_latest_version()

    if installed_version != latest_version:
        print(
            f"New version available: {latest_version}. Installed version: {installed_version}."
        )
        print("Updating Wayland...")
        build_and_install()
        build_and_install_wayland_protocols()  # Install wayland-protocols
        cleanup()  # Clean up after successful installation
    else:
        print("Wayland is up-to-date.")
        cleanup()  # Clean up if no update was necessary


if __name__ == "__main__":
    main()
